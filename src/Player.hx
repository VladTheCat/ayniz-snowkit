import luxe.Sprite;
import luxe.Vector;
import phoenix.Texture;
import luxe.Entity;
import luxe.Input;
import luxe.utils.Maths;
import luxe.components.sprite.SpriteAnimation;
import Bullet;


class Player extends Sprite
{


	public var dir 				: Int;
	private var move 			: Vector;
	private var gravity 		: Vector;
	private var shootPos		: Vector;
	private var acceleration 	: Float;
	private var topAcceleration : Float;
	private var maxSpeed 		: Float;
	private var shootAlarm 		: Float;
	private var fireBack		: Sprite;
	private var fireBottom		: Sprite;
	private var fireBckAnim		: SpriteAnimation;
	private var fireBotAnim		: SpriteAnimation;


	override function init()
	{

	}

	public function new(x : Float, y : Float)
	{
		super({});
		pos.x = x;
		pos.y = y;
		this.shootPos = new Vector(40, 14);
		this.gravity = new Vector(0, 1000);
		this.move = new Vector(0, 0);
		this.acceleration = 700;
		this.topAcceleration = 1000;
		this.maxSpeed = 800;
		this.dir = 1;
		this.shootAlarm = 0;
		//this.add(new Player(0, 0));
		//this.pause = false;

		var img = Luxe.resources.texture(Main.PlayerIMG);
		var fireImg = Luxe.resources.texture("assets/fires.png");
		img.filter_min = img.filter_mag = FilterType.nearest;
		fireImg.filter_min = fireImg.filter_mag = FilterType.nearest;

		texture = img;
		depth = 0;
		size.set_xy(img.width * 2, img.height * 2);
		fireBack = new Sprite({});
		fireBack.texture = fireImg;
		fireBack.pos.x = 100;
		fireBack.pos.y = 100;
		fireBack.size.x = fireImg.width * 2 / 5;
		fireBack.size.y = fireImg.height * 2;

		fireBottom = new Sprite({});
		fireBottom.texture = fireImg;
		fireBottom.pos.x = 120;
		fireBottom.pos.y = 120;
		fireBottom.rotation_z = -90;
		fireBottom.size.x = fireImg.width * 2 / 5;
		fireBottom.size.y = fireImg.height * 2;

		var anim_object = Luxe.resources.json("assets/fires.json");

		fireBckAnim = fireBack.add(new SpriteAnimation({name : "Test"}));
		fireBckAnim.add_from_json_object(anim_object.asset.json);
		fireBckAnim.animation = "large";
		fireBckAnim.play();

		fireBotAnim = fireBottom.add(new SpriteAnimation({name : "Test"}));
		fireBotAnim.add_from_json_object(anim_object.asset.json);
		fireBotAnim.animation = "small";
		fireBotAnim.play();
	} // new

	override function onkeydown(e : KeyEvent)
	{
		if (e.keycode == Key.key_r)
		{
			this.scene.remove(this);
			this.destroy();
		}
	} // onkeydown

	override function update(dt : Float)
	{
		shootAlarm -= dt;
		this.dir = 0;
		var moveUp = false;
		var holdAlt = true;
		var moveForward = false;
		/*this.move.x += this.gravity.x * dt;
		this.move.y += this.gravity.y * dt;*/
		if (Luxe.input.inputdown("left"))
			this.dir = -1;
		if (Luxe.input.inputdown("right"))
			this.dir = 1;
		if (Luxe.input.inputdown("up"))
		{
			moveUp = true;
			holdAlt = false;
		}
		if (Luxe.input.inputdown("down"))
		{
			holdAlt = false;
			moveUp = false;
		}

		moveForward = (this.dir != 0);
		var moveY : Float = 0;


		moveY = this.gravity.y;
		if (holdAlt)
			moveY = -this.move.y * 3;
		else if (moveUp)
			moveY = -this.topAcceleration;

		this.move.y += moveY * dt;
		

		if (Luxe.input.inputdown("action") && shootAlarm <= 0)
		{
			var shootDir : Int = if (flipx) -1 else 1;
			var bdir = new Vector(shootDir, Math.random() * 0.2 - 0.1);
			bdir.normalize();
			new Bullet(this.pos.x + this.shootPos.x * shootDir, this.pos.y + this.shootPos.y, bdir, 1200, 1);
			shootAlarm = 0.05;
		}


		move.x += acceleration * dt * dir;
		//move.y -= (Math.abs(move.x / maxSpeed * gravity.y)) * dt;
		if (this.dir != 0)
		{
			//move.y -= if (dir == Maths.sign(move.x)) move.y * 3 * dt else 0;
			flipx = dir < 0;
			fireBack.flipx = flipx;
		}
		else
		{
			move.x -= (move.x / 2) * dt;
		}

		move.x = luxe.utils.Maths.clamp(move.x, -maxSpeed, maxSpeed);
		move.y = luxe.utils.Maths.clamp(move.y, -800, 800);
		pos.x += move.x * dt;
		pos.y += move.y * dt;

		fireBack.pos.set_xy(pos.x - 52 * dir, pos.y + 2);
		fireBottom.pos.set_xy(pos.x, pos.y + 40);

		fireBack.visible = moveForward;
		fireBottom.visible = (moveUp || holdAlt);
		var f = fireBotAnim;
		if (holdAlt)
		{
			if (f.animation != "small")
				f.animation = "small";
		}
		else
		{
			if (f.animation != "large")
				f.animation = "large";
		}


		var w = Luxe.screen.width;
		var h = Luxe.screen.height;
		if (pos.x > w) pos.x -= w;
		if (pos.x < 0) pos.x += w;
		if (pos.y > h) pos.y -= h;
		if (pos.y < 0) pos.y += h;
		//this.sprite.pos = this.pos;
	} // update


}