import luxe.Sprite;
import luxe.Vector;
import phoenix.Texture;
import luxe.Entity;
import luxe.Input;
import luxe.utils.Maths;


class Bullet extends Sprite {


	private var dir			: Vector;
	private var spd			: Float;
	private var lt 			: Float;
	private static var img 	: Texture;


	public static function ready() {
		Bullet.img = Luxe.resources.texture(Main.BulletIMG);
		img.filter_min = img.filter_mag = FilterType.nearest;
	}

	override function init() {

	} // init

	public function new(x : Float, y : Float, direction : Vector, speed : Float, ?lifetime : Float = -1) {
		super({});
		pos.x = x;
		pos.y = y;
		dir = new Vector(direction.x, direction.y);
		spd = speed;
		lt = lifetime;
		texture = img;
		size.x = img.width * 3;
		size.y = img.height * 2;
		flipx = dir.x < 0;
	} // new

	override function update(dt : Float) {
		if (lt != -1)
		{
			lt -= dt;
			if (lt <= 0)
			{
				scene.remove(this);
				this.destroy();
			}
		}
		pos.x += dir.x * spd * dt;
		pos.y += dir.y * spd * dt;
	} // update


}