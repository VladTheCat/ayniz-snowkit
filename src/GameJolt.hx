package ;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.events.TimerEvent;
import flash.Lib;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.utils.Timer;
import haxe.Md5;

/**
 * GameJolt API implementation for Haxe.
 * @version 1.0.0 / Aug 14, 2013
 * @author YellowAfterlife
 */
class GameJolt {
	private static var baseURL:String = "http://gamejolt.com/api/game/v1";
	private static var privateKey:String;
	private static var gameId:Int;
	public static var userName:String = null;
	private static var userToken:String = null;
	public static var loggedIn(default, null):Bool;
	//
	private static var sessionTimer:Timer = null;
	public static var sessionActive:Bool = true;
	/**
	 * Initialisation function. Call to setup your game information before anything else.
	 * @param	gameId	Game ID
	 * @param	privateKey	Game's private key (do not share!)
	 */
	public static function init(gameId:Int, privateKey:String) {
		GameJolt.gameId = gameId;
		GameJolt.privateKey = privateKey;
	}
	//
	@:extern private static inline function wrap(text:String):String {
		return StringTools.urlEncode(text);
	}
	
	private static var keypairIdent:String = null;
	private static var keypairSpace:String = null;
	/**
	 * Inner function, allows iterating through keypairs returned by GJAPI.
	 * @param	text	Source text
	 * @param	handler	Handler (key-value)
	 */
	public static function parseKeypairs(text:String, handler:String->String->Void) {
		var p:Int = -1, l:Int = text.length, q:Int,
			c:String, o:String,
			key:String, value:String;
		//
		if (keypairIdent == null) {
			keypairIdent = "_";
			for (i in "0".code ... "9".code + 1) keypairIdent += String.fromCharCode(i);
			for (i in "a".code ... "z".code + 1) keypairIdent += String.fromCharCode(i);
			for (i in "A".code ... "Z".code + 1) keypairIdent += String.fromCharCode(i);
			keypairSpace = "\r\n\t ";
		}
		//
		while (++p < l) {
			while (p < l && keypairSpace.indexOf(c = text.charAt(p)) >= 0) p++;
			if (p >= l) break;
			if (keypairIdent.indexOf(c) < 0) break;
			q = p;
			while (keypairIdent.indexOf(c = text.charAt(p)) >= 0) p++;
			key = text.substring(q, p);
			while (keypairSpace.indexOf(c = text.charAt(p)) >= 0) p++;
			if (c != ":") break;
			p++;
			while (keypairSpace.indexOf(c = text.charAt(p)) >= 0) p++;
			if (c == "\"" || c == "'") {
				o = c;
				value = "";
				while (p < l) switch (c = text.charAt(++p)) {
					case "\\": switch (c = text.charAt(++p)) {
						case "n": value += "\n";
						case "r": value += "\r";
						}
					default:
						if (o == c) break;
						value += c;
				}
				handler(key, value);
			} else break;
		}
	}
	
	private static function makeRequest(adr:String, par:String,
	signed:Bool = true, ?format:String):URLLoader {
		var url:String = baseURL + adr + "?game_id=" + gameId;
		if (format == null) format = "dump";
		url += "&format=" + format;
		if (par != null) url += par;
		if (signed && userToken != null) url += "&username=" + wrap(userName)
			+ "&user_token=" + wrap(userToken);
		url += "&signature=" + Md5.encode(url + privateKey);
		//
		//Lib.trace(url);
		var request:URLRequest = new URLRequest(url);
		request.method = URLRequestMethod.POST;
		//
		var loader:URLLoader = new URLLoader(request);
		loader.load(request);
		return loader;
	}
	
	/**
	 * Inner function. Makes a request
	 * @param	url	Relative request URL
	 * @param	par	Parameters (&key=value&...)
	 * @param	handler	Response handler (returned text)
	 * @param	?sign	Whether request should be signed with current credentials
	 * @param	?fmt	Desired response format
	 */
	public static function request(url:String, par:String, handler:String->Void, ?sign:Bool, ?fmt:String) {
		var loader = makeRequest(url, par, sign, fmt),
		unbind = null, onData, onError;
		loader.addEventListener(Event.COMPLETE, onData = function(e) {
			handler(loader.data);
			unbind();
		});
		loader.addEventListener(IOErrorEvent.IO_ERROR, onError = function(e) {
			handler("");
			unbind();
		});
		loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
		//
		unbind = function() {
			loader.removeEventListener(Event.COMPLETE, onData);
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
			//loader.close();
		}
	}
	
	/**
	 * Shortcut for signed requests with GameJolt.request()
	 */
	public static function srequest(url:String, par:String, handler:Dynamic->Void, ?fmt:String) {
		request(url, par, handler, true, fmt);
	}
	
	/**
	 * Shortcut for unsigned requests with GameJolt.request()
	 */
	public static function urequest(url:String, par:String, handler:Dynamic->Void, ?fmt:String) {
		request(url, par, handler, false, fmt);
	}
	
	/**
	 * Attempts to either login with given credentials, or sets up guest name.
	 * @param	handler	Response handler (success/failure)
	 * @param	username	Username
	 * @param	?userToken	Token. If left empty (null), will set API to use guest name.
	 */
	public static function auth(handler:Bool->Void, username:String, ?userToken:String) {
		GameJolt.userName = username;
		loggedIn = false;
		if ((GameJolt.userToken = userToken) != null)
		srequest("/users/auth/", "", function(data) {
			loggedIn = (data.indexOf("SUCCESS") == 0);
			if (handler != null) handler(loggedIn);
			if (!loggedIn) GameJolt.userToken = null;
		});
	}
	
	/**
	 * Opens (or reopens) a game session.
	 * @param	handler	Response handler (success/failure)
	 * @param	autoPing	Whether session pings should be sent automatically.
	 */
	public static function sessionOpen(handler:Bool->Void = null, autoPing:Bool = true) {
		if (loggedIn) srequest("/sessions/open/", "", function(data) {
			var success:Bool = (data.indexOf("SUCCESS") == 0);
			if (handler != null) handler(success);
			if (success && autoPing) {
				if (sessionTimer == null) {
					sessionTimer = new Timer(30 * 1000);
					sessionTimer.addEventListener(TimerEvent.TIMER, function(e) {
						sessionPing();
					});
				}
				//
				sessionTimer.start();
			}
		});
	}
	
	/**
	 * Sends a session ping. It's said that these should occur at least once per minute for
	 * user to be marked as in progress of game session.
	 * @param	?handler	
	 * @param	?active	
	 */
	public static function sessionPing(?handler:Bool->Void, ?active:Bool) {
		if (active == null) active = sessionActive;
		if (loggedIn) srequest("/sessions/ping/", "&status=" + (active ? "active" : "idle"), function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		});
	}
	
	/**
	 * Closes currently open session, if any. Calling this method at game end is
	 * optional - unclosed sessions are closed automatically within two minutes.
	 * @param	handler	Response handler (success/failure)
	 */
	public static function sessionClose(handler:Bool->Void = null) {
		if (sessionTimer != null && sessionTimer.running) sessionTimer.stop();
		if (loggedIn) srequest("/sessions/close/", "", function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		});
	}
	
	/**
	 * Submits score to default (primary) high score table.
	 * For custom highscore tables, see scoreAddTo.
	 * @param	score	Numeric score value (used for sorting)
	 * @param	?handler	Response handler (success/failure)
	 * @param	?scoreText	Visible score text
	 * @param	?extraData	Extra data
	 */
	public static function scoreAdd(score:Float, ?handler:Bool->Void, ?scoreText:String, ?extraData:String) {
		scoreAddTo( -1, score, handler, scoreText, extraData);
	}
	
	/**
	 * Submits score to specified high score table.
	 * @param	table	Table ID (as set in High Scores tab)
	 * @param	score	Numeric score value (used for sorting)
	 * @param	?handler	Response handler (success/failure)
	 * @param	?scoreText	Visible score text
	 * @param	?extraData	Extra data
	 */
	public static function scoreAddTo(table:Int, score:Float, ?handler:Bool->Void,
	?scoreText:String, ?extraData:String) {
		if (scoreText == null) scoreText = Std.string(score);
		var par = "&sort=" + wrap(Std.string(score)) + "&score=" + wrap(scoreText);
		if (table != -1) par += "&table_id=" + table;
		if (extraData != null) par += "&extra_data=" + wrap(extraData);
		if (!loggedIn) par += "&guest=" + wrap(userName);
		srequest("/scores/add/", par, function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		});
	}
	
	
	private static function scoreGetExt(table:Int, handler:Array<ScoreEntry>->Void,
	limit:Int = 0, pool:Array<ScoreEntry>, own:Bool) {
		var par = "", hdl;
		if (table != -1) par += "&table_id=" + table;
		if (limit > 0) par += "&limit=" + limit;
		request("/scores/", par, function(data:String) {
			var index = -1, entry:ScoreEntry = null;
			if (pool == null) pool = [];
			parseKeypairs(data, function(k, v) {
				switch (k) {
				case 'score':
					if (++index >= pool.length) {
						pool[index] = entry = new ScoreEntry();
					} else entry = pool[index];
					entry.scoreText = v;
				case 'sort': entry.score = Std.parseFloat(v);
				case 'extra_data': entry.extraData = v;
				case 'user': entry.name = v;
				case 'user_id': entry.id = Std.parseInt(v);
				case 'guest': if (v != '') { entry.name = v; entry.id = -1; }
				case 'stored': entry.stored = v;
				}
			});
			// slice off extra entries from end of pool:
			if (++index < pool.length) pool.splice(index, pool.length - index);
			handler(pool);
		}, own, "keypair");
	}
	
	/**
	 * Retrieves a list of score entries for default table.
	 * @param	handler	Response handler (array of score entries)
	 * @param	limit	Maximum number of entries to be fetched (1-100, 0 for default amount)
	 * @param	?pool	Existing array of values (optional)
	 */
	public static inline function scoreGet(handler:Array<ScoreEntry>->Void,
	limit:Int = 0, ?pool:Array<ScoreEntry>) {
		scoreGetExt(-1, handler, limit, pool, false);
	}
	
	/**
	 * Retrieves a list of score entries for given table.
	 * @param	table	Table index
	 * @param	handler	Response handler (array of score entries)
	 * @param	limit	Maximum number of entries to be fetched (1-100, 0 for default amount)
	 * @param	?pool	Existing array of values (optional)
	 */
	public static inline function scoreGetFrom(table:Int, handler:Array<ScoreEntry>->Void,
	limit:Int = 0, ?pool:Array<ScoreEntry>) {
		scoreGetExt(table, handler, limit, pool, false);
	}
	
	/**
	 * Retrieves a list of score entries by current user in default table.
	 * @param	handler	Response handler (array of score entries)
	 * @param	limit	Maximum number of entries to be fetched (1-100, 0 for default amount)
	 * @param	?pool	Existing array of values (optional)
	 */
	public static inline function scoreGetOwn(handler:Array<ScoreEntry>->Void,
	limit:Int = 0, ?pool:Array<ScoreEntry>) {
		scoreGetExt(-1, handler, limit, pool, true);
	}
	
	/**
	 * Retrieves a list of score entries by current user in given table.
	 * @param	table	Table index
	 * @param	handler	Response handler (array of score entries)
	 * @param	limit	Maximum number of entries to be fetched (1-100, 0 for default amount)
	 * @param	?pool	Existing array of values (optional)
	 */
	public static inline function scoreGetOwnFrom(table:Int, handler:Array<ScoreEntry>->Void,
	limit:Int = 0, ?pool:Array<ScoreEntry>) {
		scoreGetExt(table, handler, limit, pool, true);
	}
	
	/**
	 * Retrieves a list of score tables available for the game.
	 * @param	handler	Response handler (array of score tables)
	 * @param	?pool	Existing array of values (optional)
	 */
	public static function scoreGetTables(handler:Array<ScoreTable>->Void, ?pool:Array<ScoreTable>) {
		urequest("/scores/tables/", "", function(data:String) {
			var idx:Int = -1, table:ScoreTable = null;
			if (pool == null) pool = [];
			parseKeypairs(data, function(k, v) {
				switch (k) {
					case 'id':
						if (++idx >= pool.length) {
							pool[idx] = table = new ScoreTable();
						} else table = pool[idx];
						table.id = Std.parseInt(v);
					case 'name':
						table.name = v;
					case 'description':
						table.description = v;
					case 'primary':
						table.primary = (v == '1' || v == 'true');
				}
			});
			if (++idx < pool.length) pool.splice(idx, pool.length - idx);
			handler(pool);
		}, "keypair");
	}
	
	private static function storeGetExt(key:String, handler:String->Void, own:Bool) {
		request("/data-store/", "&key=" + wrap(key), function(data) {
			if (handler != null) handler((data.indexOf("SUCCESS") == 0)
				? data.substr(data.charAt(7) == "\r" ? 9 : 8)
				: null);
		}, own);
	}
	
	/**
	 * Retrieves a value from global data store.
	 * @param	key
	 * @param	handler
	 */
	public static function storeGet(key:String, handler:String->Void) {
		storeGetExt(key, handler, false);
	}
	
	/**
	 * Retrieves a value from private data store for current user.
	 * @param	key
	 * @param	handler
	 */
	public static function storeGetOwn(key:String, handler:String->Void) {
		storeGetExt(key, handler, true);
	}
	
	private static function storeSetExt(key:String, value:String, handler:Bool->Void, own:Bool) {
		request("/data-store/set/", "&key=" + wrap(key) + "&data=" + wrap(value), function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		}, own);
	}
	
	/**
	 * Sets a value in global data store.
	 * @param	key
	 * @param	value
	 * @param	?handler
	 */
	public static inline function storeSet(key:String, value:String, ?handler:Bool->Void) {
		storeSetExt(key, value, handler, false);
	}
	
	/**
	 * Sets a value in private data store for current user.
	 * @param	key
	 * @param	value
	 * @param	?handler
	 */
	public static inline function storeSetOwn(key:String, value:String, ?handler:Bool->Void) {
		storeSetExt(key, value, handler, true);
	}
	
	private static function storeUpdateExt(key:String, operation:String, value:String, 
	handler:String->Void, own:Bool) {
		request("/data-store/update", "&key=" + wrap(key) + "&value=" + wrap(value)
		+ "&operation=" + wrap(operation), function(data) {
			if (handler != null) handler((data.indexOf("SUCCESS") == 0)
				? data.substr(data.charAt(7) == "\r" ? 9 : 8)
				: null);
		}, own);
	}
	
	/**
	 * Updates a value in global data store.
	 * @param	key
	 * @param	operation
	 * @param	value
	 * @param	?handler
	 */
	public static inline function storeUpdate(key:String, operation:String, value:String,
	?handler:String->Void) {
		storeUpdateExt(key, operation, value, handler, false);
	}
	
	/**
	 * Updates a value in private data store for current user.
	 * @param	key
	 * @param	operation
	 * @param	value
	 * @param	?handler
	 */
	public static inline function storeUpdateOwn(key:String, operation:String, value:String,
	?handler:String->Void) {
		storeUpdateExt(key, operation, value, handler, true);
	}
	
	private static function storeRemoveExt(key:String, handler:Bool->Void, own:Bool) {
		request("/data-store/remove/", "&key=" + wrap(key), function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		}, own);
	}
	
	/**
	 * Removes a value in global data store.
	 * @param	key
	 * @param	?handler
	 */
	public static inline function storeRemove(key:String, ?handler:Bool->Void) {
		storeRemoveExt(key, handler, false);
	}
	
	/**
	 * Removes a value in private data store for current user.
	 * @param	key
	 * @param	?handler
	 */
	public static inline function storeRemoveOwn(key:String, ?handler:Bool->Void) {
		storeRemoveExt(key, handler, false);
	}
	
	private static function storeGetKeysExt(handler:Array<String>->Void, pool:Array<String>, own:Bool) {
		request("/data-store/get-keys/", "", function(data) {
			if (pool == null) pool = [];
			var index:Int = 0;
			parseKeypairs(data, function(k, v) { switch (k) {
				case 'key': pool[index++] = v;
			} } );
			if (index < pool.length) pool.splice(index, pool.length - index);
			handler(pool);
		}, own, "keypair");
	}
	
	/**
	 * Retrieves a list of global data store keys.
	 * @param	handler	Response handler (array of key names)
	 * @param	?pool	
	 */
	public static inline function storeGetKeys(handler:Array<String>->Void, ?pool:Array<String>) {
		storeGetKeysExt(handler, pool, false);
	}
	
	/**
	 * Retrieves a list of data store keys for current user.
	 * @param	handler	Response handler (array of key names)
	 * @param	?pool	
	 */
	public static inline function storeGetKeysOwn(handler:Array<String>->Void, ?pool:Array<String>) {
		storeGetKeysExt(handler, pool, true);
	}
	
	private static function trophyGetExt(handler:Dynamic->Void, _pool:Dynamic,
	list:String, achieved:Null<Bool>, one:Bool) {
		var par:String = "";
		if (list != null) par += "&trophy_id=" + wrap(list);
		if (achieved != null) par += "&achieved=" + (achieved ? "true" : "false");
		srequest("/trophies/", par, function(data) {
			var trophy:Trophy = null;
			var index:Int = -1;
			var pool:Array<Trophy> = one ? null : _pool == null ? [] : cast _pool;
			parseKeypairs(data, function(k, v) { switch (k) {
				case 'id':
					if (!one) {
						if (++index >= pool.length) {
							pool[index] = trophy = new Trophy();
						} else trophy = pool[index];
					} else trophy = (_pool != null) ? cast _pool : new Trophy();
					trophy.id = Std.parseInt(v);
				case 'title': trophy.title = v;
				case 'description': trophy.description = v;
				case 'difficulty': trophy.difficulty = v;
				case 'image_url': trophy.imageURL = v;
				case 'achieved': trophy.achieved = v == "true";
			} } );
			if (!one && _pool != null && ++index < pool.length) {
				pool.splice(index, pool.length - index);
			}
			handler(one ? trophy : pool);
		}, "keypair");
	}
	
	/**
	 * Retrieves information about specified trophy.
	 * If trophy was not found, null will be passed to handler.
	 * @param	id	Trophy ID (as seen on dashboard)
	 * @param	handler	
	 * @param	?pool
	 */
	public static inline function trophyGet(id:Int, handler:Trophy->Void, ?pool:Trophy) {
		trophyGetExt(handler, pool, "" + id, null, true);
	}
	
	/**
	 * Fetches information about given list of trophies
	 * @param	ids	Array of trophy IDs
	 * @param	handler
	 * @param	?pool
	 */
	public static inline function trophyFetch(ids:Array<Int>, handler:Array<Trophy>->Void,
	?pool:Array<Trophy>) {
		trophyGetExt(handler, pool, ids.join(","), null, false);
	}
	
	/**
	 * Fetches information about all trophies present in the game
	 * @param	handler	
	 * @param	?achieved
	 * @param	?pool
	 */
	public static inline function trophyList(handler:Array<Trophy>->Void, ?achieved:Bool,
	?pool:Array<Trophy>) {
		trophyGetExt(handler, pool, null, achieved, false);
	}
	
	/**
	 * Marks given trophy as achieved for player.
	 * @param	id	Trophy ID
	 * @param	?handler
	 */
	public static function trophyAdd(id:Int, ?handler:Bool->Void) {
		srequest("/trophies/add-achieved/", "&trophy_id=" + id, function(data) {
			if (handler != null) handler(data.indexOf("SUCCESS") == 0);
		});
	}
	
	private static function userGetExt(handler:Dynamic->Void, _pool:Dynamic, param:String, 
	mode:Null<Bool>) {
		var par:String = "";
		urequest("/users/", (mode == null ? "&username=" : "&user_id=") + wrap(param), function(data) {
			var info:UserInfo = null;
			var index:Int = -1;
			var pool:Array<UserInfo> = (mode != true) ? null : _pool == null ? [] : cast _pool;
			parseKeypairs(data, function(k, v) { switch (k) {
				case 'id':
					if (mode == true) {
						if (++index >= pool.length) {
							pool[index] = info = new UserInfo();
						} else info = pool[index];
					} else info = (_pool != null) ? cast _pool : new UserInfo();
					info.id = Std.parseInt(v);
				case 'type': info.type = v;
				case 'username': info.name = v;
				case 'avatar_url': info.avatar = v;
				case 'signed_up': info.signedUp = v;
				case 'status': info.active = (v == 'Active');
				case 'developer_name': info.devName = v;
				case 'developer_website': info.devURL = v;
				case 'developer_description': info.devText = v;
			} } );
			if (mode == true && _pool != null && ++index < pool.length) {
				pool.splice(index, pool.length - index);
			}
			handler(mode != true ? info : pool);
		}, "keypair");
	}
	
	/**
	 * Gets information about specified user.
	 * @param	id	User ID
	 * @param	handler
	 * @param	?pool
	 */
	public static function userGet(id:Int, handler:UserInfo->Void, ?pool:UserInfo) {
		userGetExt(handler, pool, "" + id, false);
	}
	
	/**
	 * Attempts to find specified user by name.
	 * If user is not found, null will be passed to handler.
	 * @param	name	User name
	 * @param	handler
	 * @param	?pool
	 */
	public static function userFind(name:String, handler:UserInfo->Void, ?pool:UserInfo) {
		userGetExt(handler, pool, name, null);
	}
	
	/**
	 * Retrieves information about specified users.
	 * @param	ids	Array of user IDs
	 * @param	handler
	 * @param	?pool
	 */
	public static function userFetch(ids:Array<Int>, handler:Array<UserInfo>->Void,
	?pool:Array<UserInfo>) {
		userGetExt(handler, pool, ids.join(","), true);
	}
}

/**
 * Represents information about user.
 * Commonly returned by user* functions.
 */
class UserInfo {
	public var id:Int;
	public var type:String;
	public var name:String;
	public var avatar:String;
	public var signedUp:String;
	public var lastLogin:String;
	public var active:Bool;
	public var devName:String;
	public var devURL:String;
	public var devText:String;
	public function new() { }
}

/**
 * Represents info about single achievable trophy.
 */
class Trophy {
	public var id:Int;
	public var title:String;
	public var description:String;
	public var difficulty:String;
	public var imageURL:String;
	public var achieved:Bool;
	public function new() { }
}

/**
 * Represents information about a highscore table.
 */
class ScoreTable {
	public var id:Int;
	public var name:String;
	/** Table description (optional) */
	public var description:String;
	/** Whether table is the primary highscore table */
	public var primary:Bool;
	public function new() { }
}

/**
 * Represents information about single highscore entry.
 */
class ScoreEntry {
	/** Numeric score value */
	public var score:Float;
	/** Actual score text (as seen on site) */
	public var scoreText:String;
	/** Extra score data, as set in functions */
	public var extraData:String;
	/** Displayed user name. */ 
	public var name:String;
	/** User id. -1 if guest.*/
	public var id:Int;
	/** Relative date-time of when score was submitted. */
	public var stored:String;
	public function new() { }
}