import luxe.Input;
import luxe.Color;
import luxe.Entity;
import luxe.Vector;
import luxe.Sprite;
import luxe.Text;
import phoenix.Texture;
import luxe.Parcel;
import luxe.ParcelProgress;
import snow.types.Types;
import Player;
import Bullet;


class Main extends luxe.Game 
{


	public var player : Player;


	public static inline var PlayerIMG = "assets/ship.png";
	public static inline var BulletIMG = "assets/bullet.png";


	/*override function config(config:luxe.AppConfig)
	{
		config.preload.textures.push({id : PlayerIMG});
		config.preload.textures.push({id : BulletIMG});

		return config;
	}// config*/

	override function ready() 
	{
		var parcel = new Parcel({
			jsons : [
				{id : "assets/fires.json"}
			],
			textures : [
				{id : "assets/ship.png"},
				{id : "assets/bullet.png"},
				{id : "assets/fires.png"}
			]
		});

		new ParcelProgress({
            parcel      : parcel,
            background  : new Color(1,1,1,0.85),
            oncomplete  : assets_loaded
        });

            //go!
        parcel.load();
    }

    public function assets_loaded(_){
		player = new Player(100, 100);
        Bullet.ready();
		
		connect_input();
	}// ready

	override function onkeydown(e:KeyEvent) 
	{

	}// onkeydown

	override function onkeyup(e:KeyEvent)
	{
		
	}// onkeyup

	function connect_input()
	{
		Luxe.input.bind_key('left', Key.left);
        Luxe.input.bind_key('left', Key.key_a);

        Luxe.input.bind_key('right', Key.right);
        Luxe.input.bind_key('right', Key.key_d);

        Luxe.input.bind_key('up', Key.up);
        Luxe.input.bind_key('down', Key.down);
        Luxe.input.bind_key('action', Key.key_x);
    }// connect_input

	override function update(dt:Float) 
	{

	}// update


}
